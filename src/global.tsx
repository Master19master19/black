import * as Localization from 'expo-localization';
import i18n from 'i18n-js';
import * as React from "react";
import { AsyncStorage , Text , Alert , Dimensions } from 'react-native';
// import NavigationService from '@app/navigation/Root';



global.logOut = async() => {
    this.isAuth = false;
    await AsyncStorage.clear();
}

global.smallSize = () => {
    let width = Dimensions.get( 'window' ).width;
    // console.log(width)
    return width <= 375;
}


global.getHeader = async () => {
    let token = await global.getToken();
    if ( ! token ) {
        return {};
    }
    return {
        headers: {
            Authorization: 'Bearer ' + token
        }
    };
}

global.handleError = ( error ) => {
    if ( undefined != error.response && undefined !== error.response.data ) {
        console.log({ errrarrraaaa: error.response.data });
    }
    console.log({ handleError: error});
    let errorTexts;
    if ( error.response.status == 302 ) {
        this.props.navigation.navigate( "Auth.ConfirmEmail1Reg" , { phone: this.state.phone, action: 'register' } );
    } else {
        try {
            if ( undefined !== error.response.data.error ) {
                errorTexts = generateErrors( { 'key': [ error.response.data.error ] } );
            } else {
                errorTexts = generateErrors( error.response.data.errors );
            }
        } catch ( e ) {
            console.log( 'Error' , e );
        }
    }
}


global.log = ( ...params ) => {
    // params.forEach( ( val , k ) => {
    // 	alert( val );
    // });
    return true;
}

global.handlePush = () => {
    registerForPushNotificationsAsync();
}

global.API_ADDRESS = "https://fitness.tennis.ru";

const ruShortDays = [
    'Вс',
    'Пн',
    'Вт',
    'Ср',
    'Чт',
    'Пт',
    'Сб',
];



global.cleanCart = async() => {
    await AsyncStorage.removeItem( 'cart.seats' );
    await AsyncStorage.removeItem( 'cart.subscriptions' );
    await AsyncStorage.setItem( 'cartNumber' , "0" );
    global.cartNumber = 0;
    if ( undefined !== global.cartt && null !== global.cartt ) {
        global.cartt.setState({ number: 0 });
    }
}
global.logOut = async() => {
    this.isAuth = false;
    await AsyncStorage.clear();
    await global.setCartNumber();
}
global.setCartNumber = async () => {
    let seats = await AsyncStorage.getItem( 'cart.seats' ) ?? "{}";
    seats = JSON.parse( seats );
    let count = Object.keys( seats ).length;
    let subscriptions = await AsyncStorage.getItem( 'cart.subscriptions' ) ?? "{}";
    subscriptions = JSON.parse( subscriptions );
    count += Object.keys( subscriptions ).length;
    // console.log({caaa:global.cartNumber})
    global.cartNumber = count;
    if ( undefined !== global.cartt && null !== global.cartt ) {
        global.cartt.setState({ number: count });
    }
    await AsyncStorage.setItem( 'cartNumber' , count + "" );
}

global.getCartNumber = async () => {
    let count = await AsyncStorage.getItem( 'cartNumber' );
    if ( null == count ) count = 0;
    // console.log({cartCount:count})
    return count;
}

global.smallSize = () => {
    let width = Dimensions.get( 'window' ).width;
    // console.log({width})
    return width <= 350;
}

global.getWeekDay = ( date ) => {
    date = moment( date ).day();
    return ruShortDays[ date ];
}


global.getHeader = async () => {
    let token = await global.getToken();
    if ( ! token ) {
        return {};
    }
    return {
        headers: {
            Authorization: 'Bearer ' + token
        }
    };
}

global.handleError = async ( error ) => {
    if ( undefined != error.response && undefined !== error.response.data ) {
        console.log({ errrarrraaaa: error.response.data });
    }
    console.log({ handleError: error});
    let errorTexts;
    if ( error.response.status == 302 ) {
        this.props.navigation.navigate( "Auth.ConfirmEmail1Reg" , { phone: this.state.phone, action: 'register' } );
    } else if ( error.response.status == 425 ) {
        errorTexts = generateErrors( { 'key': [ error.response.data.error ] } );
        global.isAuth = false;
        await AsyncStorage.clear();
        NavigationService.navigate('Auth.Base' );
    } else if ( error.response.status == 426 ) {
        errorTexts = generateErrors( { 'key': [ error.response.data.error ] } );
        NavigationService.navigate( 'Profile.Cards' );
    } else {
        try {
            if ( undefined !== error.response.data.error ) {
                errorTexts = generateErrors( { 'key': [ error.response.data.error ] } );
            } else {
                errorTexts = generateErrors( error.response.data.errors );
            }
        } catch ( e ) {
            console.log( 'Error' , e );
        }
    }
}


global.Alert = ( title , type = 'error' ) => {
    let heading = 'Ошибка!';
    if ( type !== 'error' ) {
        heading = '';
    }
    Alert.alert(
        heading,
        title,
        [
            { text: 'OK' },
        ]
    );
}
function generateErrors ( errors ) {
    let errText = [];
    console.log(errors)
    for ( let key in errors ) {
        let err = errors[ key ][ 0 ].toString();
        errText.push( err + "\n" );
    }
    Alert.alert(
        'Ошибка!',
        errText[ 0 ],
        [
            { text: 'OK', onPress: () => console.log( '' ) },
        ]
    );
}


global.getToken = async () => {
    try {
        let token = await AsyncStorage.getItem( 'token' );
        if ( undefined == token || null == 'token' || token.length < 10 ) {
            return false;
        }
        return token;
    } catch ( e ) {
        return false;
    }
}
global.authenticated = async () => {
    try {
        let token = await AsyncStorage.getItem( 'token' );
        if ( undefined == token || null == 'token' || token.length < 10 ) {
            return false;
        }
        return true;
    } catch ( e ) {
        return false;
    }
}
global.cartNumber = 0;
global.test = 'initial';
global.isAuth = true;
// Set the key-value pairs for the different languages you want to support.
i18n.translations = {
    en: {
        profile: 'Profile',
        name: 'Name',
        passport: 'Passport',
        address: 'Address',
        email: 'E-mail',
        inn: 'Inn',
        company_name: 'Company name',
        language: 'Language',
        company: 'Company',
        person: 'Person',
        trucks: 'Trucks',
        passport: 'Passport',
        passport: 'Passport',
    },
    ru: {
        profile: 'Профиль',
        name: 'Имя',
        passport: 'Паспорт',
        address: 'Адрес',
        email: 'Эл. почта',
        inn: 'ИНН',
        company_name: 'Название компании',
        language: 'Язык',
        company: 'Юр. лицо',
        person: 'Физ. лицо',
        trucks: 'Грузовики',
        passport: 'Passport',
        passport: 'Passport',
    },
    am: {
        profile: 'Profile',
        name: 'Name',
        passport: 'Passport',
        address: 'Address',
        email: 'E-mail',
        inn: 'Inn',
        company_name: 'Company name',
        language: 'Language',
        passport: 'Passport',
        passport: 'Passport',
        passport: 'Passport',
        passport: 'Passport',
    },
};

// Set the locale once at the beginning of your app.
    i18n.locale = Localization.locale;
// When a value is missing from a language it'll fallback to another language with the key present.
i18n.fallbacks = true;

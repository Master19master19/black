import {Button, ScrollView, Text, TouchableOpacity, View, Image, FlatList, Dimensions} from "react-native";
import React , { useContext , useState , useRef } from "react";
import Input from '@components/Input';
import Colors from '@app/constants/Colors';
import Truck from '@img/cars/truck-15.png';
import { Entypo } from '@expo/vector-icons';


const DATA = [{"id":1,"title":"до 500кг"},
    {"id":2,"title":"до 4т"},
    {"id":3,"title":"до 5т"},
    {"id":24,"title":"до 6т"},
    {"id":1000,"title":"до 10т"}];



const WINDOW_WIDTH = Dimensions.get( 'window' ).width;


function Item ({item, index}) {
    return (
        <View style={{ width: WINDOW_WIDTH , textAlign:'center' , flexDirection: 'row' , alignItems: 'center' , justifyContent: 'center' }}>
            <Image source={Truck} style={{ width: 150 , height: 150, resizeMode: 'contain' , marginRight: 25 }} />
            <Text style={{ fontSize: 50 }}>{ item.title }</Text>
        </View>
    );
}

function Screen({navigation}) {
    const { t, locale, setLocale } = useContext(LocalizationContext);
    const [ userType , setUserType ] = useState( 'person' );
    const [ index , setIndex ] = useState( 1 );
    const list = useRef(null);

    const changePage = ( index ) => {
        if ( index >= 0 ) {
            list.current.scrollToIndex({ index: index });
            setIndex( index );
        }
    }
    const onViewableItemsChanged = useRef(({viewableItems, changed})=> {
        let newIndex = changed[0]['index'];
        setIndex(newIndex)
        console.log(newIndex)
    })
    const viewConfigRef = useRef({ viewAreaCoveragePercentThreshold: 50 })

    return (
        <View
            style={styles.wrapper}>
            <Text style={{ fontSize: 25 , textAlign: 'center' }}>{t( 'trucks' )}</Text>
            <View style={ styles.header }>
                {
                    DATA.map( ( val, key ) => {
                        return <TouchableOpacity style={[ styles.headerItem , { width: `${100 / DATA.length }%` , borderColor: index == key ? Colors.primary : Colors.default } ]} key={key.toString()} onPress={() => changePage( key )}>
                            <Text style={[{ fontSize: 15 , textAlign: 'center' }, { color: index == key ? Colors.primary : Colors.default } ]}>{val.title}</Text>
                        </TouchableOpacity>
                    })
                }
            </View>
            <View>
                {
                    index > 0 &&
                    <TouchableOpacity style={[ styles.controlIcon, { left: 0 }]} onPress={() => changePage( index-1 )}>
                        <Entypo name="chevron-left" size={34} color={Colors.primary} />
                    </TouchableOpacity>
                }
                {
                    index + 1 < DATA.length &&
                    <TouchableOpacity style={[ styles.controlIcon, { right: 0 }]} onPress={() => changePage( index+1 )}>
                        <Entypo name="chevron-right" size={34} color={Colors.primary} />
                    </TouchableOpacity>
                }
                <FlatList
                    ref={list}
                    removeClippedSubviews={true}
                    pagingEnabled={true}
                    viewabilityConfig={viewConfigRef.current}
                    snapToInterval={WINDOW_WIDTH}
                    getItemLayout={(data, index) => ({
                        length: WINDOW_WIDTH,
                        offset: WINDOW_WIDTH * index,
                        index,
                    })}
                    contentContainerStyle={{ justifyContent: 'center' }}
                    style={{ width: '100%' }}
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}
                    data={DATA}
                    renderItem={Item}
                    snapToInterval={WINDOW_WIDTH}
                    decelerationRate={0.6}
                    disableIntervalMomentum={true}
                    onViewableItemsChanged={ onViewableItemsChanged.current }
                    keyExtractor={item => item.id} />
            </View>
        </View>
    );
}
const styles = {
    header: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%'
    },
    headerItem: {
        borderBottomWidth: 3,
        textAlign: 'center',
        padding: 10,
    },
    controlIcon: {
        padding: 20,
        position: 'absolute',
        height: '100%',
        zIndex: 1500,
        alignSelf: 'center',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    dotStyle: {
        backgroundColor: 'rgba(255, 255, 255, 0.48)',
    },
    flag: {
        resizeMode: 'contain',
        width: 50,
        height: 50
    },
    activeDotStyle: {
        backgroundColor: 'white',
    },
    wrapper: {},
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5c77a9',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5c77a9',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5c77a9',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
};

export default Screen;
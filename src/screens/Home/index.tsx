import {Button, Text, View} from "react-native";
import * as React from "react";

function Screen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Home Screen</Text>
            <Button
                title="Go to Details"
                onPress={() => navigation.navigate('Details')}
            />
            <Button
                title="Go to House"
                onPress={() => navigation.navigate('House')}
            />
        </View>
    );
}


export default Screen;
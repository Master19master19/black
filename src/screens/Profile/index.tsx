import {Button, ScrollView, Text, TextInputComponent, TouchableOpacity, View,Image} from "react-native";
import React , { useContext , useState } from "react";
import Input from '@components/Input';
import Colors from '@app/constants/Colors';
import USA from '@img/flags/usa.png';
import Armenia from '@img/flags/armenia.png';
import Russia from '@img/flags/russia.png';


function Screen({navigation}) {
    const { t, locale, setLocale } = useContext(LocalizationContext);
    const [ userType , setUserType ] = useState( 'person' );
    return (
        <View
            style={styles.wrapper}>
            <Text style={{ fontSize: 25 , textAlign: 'center' }}>{t( 'profile' )}</Text>
            <View>
                <View style={{ flexDirection: 'row' , justifyContent: 'space-around' }}>
                    <TouchableOpacity onPress={()=>setUserType( 'person' )} style={{ backgroundColor: userType == 'person' ? Colors.bgDanger : Colors.bgPrimary}}>
                        <Text>{ t( 'person' ) }</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>setUserType( 'company' )} style={{ backgroundColor: userType !== 'person' ? Colors.bgDanger : Colors.bgPrimary}}>
                        <Text>{ t( 'company' ) }</Text>
                    </TouchableOpacity>
                </View>
            </View>
            {
                userType == 'person' ?
                    <View>
                        <View>
                            <Input placeHolder={t( 'name' )} autoFocus={true} />
                        </View>
                        <View>
                            <Input placeHolder={t( 'email' )} />
                        </View>
                        <View>
                            <Input placeHolder={t( 'address' )} />
                        </View>
                        <View>
                            <Input placeHolder={t( 'passport' )} />
                        </View>
                        <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
                    </View>
                    :
                    <View>
                        <View>
                            <Input placeHolder={t( 'company_name' )} autoFocus={true} />
                        </View>
                        <View>
                            <Input placeHolder={t( 'email' )} />
                        </View>
                        <View>
                            <Input placeHolder={t( 'address' )} />
                        </View>
                        <View>
                            <Input placeHolder={t( 'inn' )} />
                        </View>
                        <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
                    </View>

            }
            <View>
                <View><Text>{ t( 'language' ) }</Text></View>
            </View>
            <View>
                <View style={{ flexDirection: 'row' , justifyContent: 'space-between'}}>
                    <TouchableOpacity onPress={()=>setLocale( 'ru' )}>
                        <Image source={ Russia } style={ styles.flag } />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>setLocale( 'am' )}>
                        <Image source={ Armenia } style={ styles.flag } />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>setLocale( 'en' )}>
                        <Image source={ USA } style={ styles.flag } />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}
const styles = {
    dotStyle: {
        backgroundColor: 'rgba(255, 255, 255, 0.48)',
    },
    flag: {
        resizeMode: 'contain',
        width: 50,
        height: 50
    },
    activeDotStyle: {
        backgroundColor: 'white',
    },
    wrapper: {},
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5c77a9',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5c77a9',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#5c77a9',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
};

export default Screen;
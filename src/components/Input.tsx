import {Image, TextInput, TouchableOpacity, View,StyleSheet} from "react-native";
import * as React from "react";
import Colors from '@app/constants/Colors';

function Component( props ) {
    const { t, locale, setLocale } = React.useContext( LocalizationContext );

    return (
            <View style={[props.style,styles.wrapper]}>
                <View style={{ display: 'flex', flexDirection: 'row' }}>
                    {props.leftIcon && <Image source={props.leftIcon} resizeMethod="scale" style={styles.icon} /> }
                    <TextInput autoFocus={props.autoFocus} secureTextEntry={props.password}
                               placeholderTextColor={Colors.placeholder}
                               placeholder={props.placeHolder}
                               style={[styles.input, props.inputStyle]} />
                    { props.rightIcon &&
                        <TouchableOpacity onPress={ () => props.onRightPress() }>
                            <Image source={props.rightIcon} resizeMethod="scale" style={styles.rightIcon} />
                        </TouchableOpacity>
                    }
                </View>
            </View>
    );
}


const styles = StyleSheet.create({
    icon: {
        width: 22,
        height: 22,
        marginBottom: 14,
        marginTop: 14,
        marginLeft: 14,
    },
    rightIcon: {
        width: 22,
        height: 22,
        marginBottom: 14,
        marginTop: 14,
        marginRight: 14,
    },
    input: {
        width: '80%',
        margin: 'auto',
        paddingLeft: 10,
        height: 40,
        padding: 5,
        backgroundColor: '#fff',
        alignSelf: 'center',
        color: '#000',
        borderRadius: 7,
        fontSize: 20,
        elevation: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
    },
    wrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        marginVertical: 7
    },
    marginWrapper: {
    }
});

export default Component;
import * as React from "react";
import {TouchableOpacity} from "react-native";
import {FontAwesome} from "@expo/vector-icons";
import {useNavigation} from '@react-navigation/native';

function HeaderRight() {
    const navigation = useNavigation();

    return(
        <TouchableOpacity onPress={ () => navigation.navigate( 'Profile' ) }>
            <FontAwesome name="user" size={24} style={{ marginHorizontal : 15 }} />
        </TouchableOpacity>
    )
}

export default HeaderRight;
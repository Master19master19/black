import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons , AntDesign , FontAwesome } from '@expo/vector-icons';
import { navigationRef } from '@app/navigation/RootNavigation';
import HeaderRight from '@app/navigation/HeaderRight'
import HomeScreen  from '@app/screens/Home';
import TrucksScreen  from '@app/screens/Trucks';
import ProfileScreen  from '@app/screens/Profile';
import * as React from "react";
import {useContext} from "react";





const Stack = createStackNavigator();
const BottomTabs = createBottomTabNavigator();

function HomeStack () {
    return (
        <Stack.Navigator>
            <Stack.Screen  options={{headerRight:()=><HeaderRight/>}} name="Home" component={HomeScreen} />
            <Stack.Screen  name="House" component={HomeScreen}
                           options={{headerRight:()=><HeaderRight/>,title: 'House'}} />
        </Stack.Navigator>
    );
}
function ProfileStack () {
    return (
        <Stack.Navigator>
            <Stack.Screen options={{headerRight:()=><HeaderRight/>}} name="Profile" component={ProfileScreen} />
        </Stack.Navigator>
    );
}

function TrucksStack () {
    return (
        <Stack.Navigator>
            <Stack.Screen options={{headerRight:()=><HeaderRight/>}} name="etails" component={TrucksScreen} />
        </Stack.Navigator>
    );
}

function Navigation () {
    const { t, locale, setLocale } = useContext(LocalizationContext);
    return (
        <NavigationContainer ref={navigationRef}>
            <BottomTabs.Navigator
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                        let iconName;
                        if ( route.name === 'HomeStack' ) {
                            iconName = focused
                                ? 'ios-information-circle'
                                : 'ios-information-circle-outline';
                        } else if (route.name === 'Profile') {
                            return <FontAwesome name="user" size={size} color={color} />;
                        } else if (route.name === 'Trucks') {
                            return <FontAwesome name="truck" size={size} color={color} />;
                        } else if (route.name === 'Profile') {
                            iconName = focused ? 'ios-list' : 'ios-list';
                        }

                        // You can return any component that you like here!
                        return <Ionicons name={iconName} size={size} color={color} />;
                    },
                })}
                tabBarOptions={{
                    activeTintColor: 'tomato',
                    inactiveTintColor: 'gray',
                }}
            >
                <BottomTabs.Screen name="Trucks" options={{ title: t( 'trucks' ) }} component={TrucksStack} />
                <BottomTabs.Screen name="Profile" options={{ title: t( 'profile' ) }} component={ProfileStack} />
                <BottomTabs.Screen name="HomeStack" component={HomeStack} />
            </BottomTabs.Navigator>
        </NavigationContainer>
    );
}

export default Navigation;
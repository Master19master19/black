module.exports = function (api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
            [
                'module-resolver',
                {
                   "root": ["./src"],
                    "alias": {
                        '@app': './src',
                        '@img': './src/assets/img',
                        '@components': './src/components',
                        '@screens': './src/screens',
                    },
                },
            ],
        ],
  };
};

import * as React from 'react';
import Navigation from '@app/navigation';
import '@app/global';
import * as Localization from 'expo-localization'; // or whatever library you want
import i18n from 'i18n-js';

global.LocalizationContext = React.createContext();

function App() {
    const [locale, setLocale] = React.useState(Localization.locale);
    const localizationContext = React.useMemo(
        () => ({
            t: (scope, options) => i18n.t(scope, { locale, ...options }),
            locale,
            setLocale,
        }),
        [locale]
    );

    return (
        <LocalizationContext.Provider value={localizationContext}>
            <Navigation />
        </LocalizationContext.Provider>
    );
}


export default App;